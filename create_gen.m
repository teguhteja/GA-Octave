  %represetasi genetik
  function gen = create_gen(panjang_target)
    random_number = randi([32,126],1,panjang_target);
    gen = char(random_number);
  end