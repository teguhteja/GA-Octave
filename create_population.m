  function populasi = create_population(target, besar_populasi)
    populasi = struct();
    for i=1:besar_populasi
      populasi(i).gen = create_gen(length(target));
      populasi(i).fitness = calculate_fitness(populasi(i).gen,target);
    end
  end